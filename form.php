<html>
  <head>
    <style>
/* Сообщения об ошибках и поля с ошибками выводим с красным бордюром. */
.error {
  border: 2px solid red;
}
.error1{
  text-decoration: underline red;
}
#messages{
	margin-bottom: 10px;
	color: red;
}
form{
	border: 3px solid black;
    border-radius: 20px;
    text-align: center;
    background-color: white;
    padding-top: 10px;
    padding-bottom: 10px;
}
body{
    background: url('https://images.wallpaperscraft.ru/image/gory_skaly_ptitsy_164320_1920x1080.jpg') no-repeat center center/cover;
    max-width:400px;  
    margin: 0 auto;   
}
    </style>
  </head>
  <body>

<form id="form" action="" method="post">
<?php
if (!empty($messages)) {
  print('<div id="messages">');
  // Выводим все сообщения.
  foreach ($messages as $message) {
    print($message);
  }
  print('</div>');
}

// Далее выводим форму отмечая элементы с ошибками классом error
// и задавая начальные значения элементов ранее сохраненными.
?>
        <label>
            Введите имя<br>
            <input name="fio" <?php if ($errors['fio']) {print 'class="error"';} ?> value="<?php print $values['fio']; ?>" >
        </label> <br>

        <label>
           <br> Введите e-mail <br>
            <input name="email" <?php if ($errors['email']) {print 'class="error"';} ?> value="<?php print $values['email']; ?>" >
        </label> <br>

        <label>
           <br> Введите дату рождения <br>
            <input name="date" type="date" <?php if ($errors['date']) {print 'class="error"';} ?> value="<?php print $values['date']; ?>">
        </label> <br>

        <div >
           <br> Выберете пол <br>
           
            <label <?php if ($errors['sex']) {print 'class="error1"';} ?>>
                <input type="radio" value="m" name="sex" <?php if ($values['sex']=="m") {print 'checked';} ?>> муж
            </label>
            <label <?php if ($errors['sex']) {print 'class="error1"';} ?>>
                <input type="radio" value="f" name="sex" <?php if ($values['sex']=="f") {print 'checked';} ?>>жен
            </label>
        </div>

        <div>
          <br>  Кол-во конечностей <br>
            <label <?php if ($errors['limbs']) {print 'class="error1"';} ?>>
                <input type="radio" value="1" name="limbs" <?php if ($errors['limbs']) {print 'class="error"';} ?> <?php if ($values['limbs']=="1") {print 'checked';} ?>>1
            </label>
            <label <?php if ($errors['limbs']) {print 'class="error1"';} ?>>
                <input type="radio" value="2" name="limbs" <?php if ($values['limbs']=="2") {print 'checked';} ?>>2
            </label>
            <label <?php if ($errors['limbs']) {print 'class="error1"';} ?>>
                <input type="radio" value="3" name="limbs" <?php if ($values['limbs']=="3") {print 'checked';} ?>>3
            </label>
            <label <?php if ($errors['limbs']) {print 'class="error1"';} ?>>
                <input type="radio" value="4" name="limbs" <?php if ($values['limbs']=="4") {print 'checked';} ?>>4
            </label>
        </div>
        <div>
              <br>  Сверх<?php print $values['biography']; ?>способности:
                <br>
                <select name="abilities[]" multiple="multiple" >
                  <option value="Бессмертие" <?php if (strripos($values['abilities'], "Бессмертие")) {print 'selected';} ?>>Бессмертие</option>
                  <option value="Прохождение сквозь стены" <?php if (strripos($values['abilities'], "Прохождение сквозь стены")) {print 'selected';} ?>>Прохождение сквозь стены</option>
                  <option value="Левитация" <?php if (strripos($values['abilities'], "Левитация")) {print 'selected';} ?>>Левитация</option>
                </select><br>
        </div>
        <br> Биография<br>
        <textarea name="biography" cols="30" rows="10" <?php if ($errors['biography']) {print 'class="error"';} ?>><?php print $values['biography']; ?></textarea>

        <br>
        <label <?php if ($errors['check']) {print 'class="error1"';} ?>>
           <br> <input name="check" type="checkbox" <?php if ($errors['check']) {print 'class="error"';} ?> >
            С контрактом ознакомлен
        </label>
        <br>
       <br> <input type="submit" value="Отправить">
    </form>
      </body>
</html>