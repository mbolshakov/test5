<?php
/**
 * Реализовать возможность входа с паролем и логином с использованием
 * сессии для изменения отправленных данных в предыдущей задаче,
 * пароль и логин генерируются автоматически при первоначальной отправке формы.
 */

// Отправляем браузеру правильную кодировку,
// файл index.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  // Массив для временного хранения сообщений пользователю.
  $messages = array();

  // В суперглобальном массиве $_COOKIE PHP хранит все имена и значения куки текущего запроса.
  // Выдаем сообщение об успешном сохранении.
  if (!empty($_COOKIE['save'])) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('save', '', 100000);
    setcookie('login', '', 100000);
    setcookie('password', '', 100000);
    // Выводим сообщение пользователю.
    $messages[] = 'Спасибо, результаты сохранены.';
    // Если в куках есть пароль, то выводим сообщение.
    if (!empty($_COOKIE['password'])) {
      $messages[] = sprintf('Вы можете <a href="login.php">войти</a> с логином <strong>%s</strong>
        и паролем <strong>%s</strong> для изменения данных.',
        strip_tags($_COOKIE['login']),
        strip_tags($_COOKIE['password']));
    }
  }

  // Складываем признак ошибок в массив.
  $errors = array();
  $errors['fio'] = !empty($_COOKIE['fio_error']);

  // TODO: аналогично все поля.
  $errors['email'] = !empty($_COOKIE['email_error']);
  $errors['date'] = !empty($_COOKIE['date_error']);
  $errors['sex'] = !empty($_COOKIE['sex_error']);
  $errors['limbs'] = !empty($_COOKIE['limbs_error']);
  $errors['biography'] = !empty($_COOKIE['biography_error']);
  $errors['check'] = !empty($_COOKIE['check_error']);
  // Выдаем сообщения об ошибках.
  if (!empty($errors['fio'])) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('fio_error', '', 100000);
    // Выводим сообщение.
    $messages[] = '<div class="error">Заполните имя.</div>';
  }
  // TODO: тут выдать сообщения об ошибках в других полях.
  if (!empty($errors['email'])) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('email_error', '', 100000);
    // Выводим сообщение.
    $messages[] = '<div class="">Заполните email.</div>';
}
if (!empty($errors['date'])) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('date_error', '', 100000);
    // Выводим сообщение.
    $messages[] = '<div class="">Заполните дату рождения.</div>';
}
if (!empty($errors['sex'])) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('sex_error', '', 100000);
    // Выводим сообщение.
    $messages[] = '<div class="">Заполните пол.</div>';
}
if (!empty($errors['limbs'])) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('limbs_error', '', 100000);
    // Выводим сообщение.
    $messages[] = '<div class="">Заполните количество конечностей.</div>';
}
if (!empty($errors['biography'])) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('biography_error', '', 100000);
    // Выводим сообщение.
    $messages[] = '<div class="">Заполните биографию.</div>';
}
if (!empty($errors['check'])) {
    // Удаляем куку, указывая время устаревания в прошлом.
    setcookie('check_error', '', 100000);
    // Выводим сообщение.
    $messages[] = '<div class="">Отметьте чекбокс.</div>';
}
  // Складываем предыдущие значения полей в массив, если есть.
  // При этом санитизуем все данные для безопасного отображения в браузере.
  $values = array();
  $values['fio'] = empty($_COOKIE['fio_value']) ? '' : strip_tags($_COOKIE['fio_value']);
  // TODO: аналогично все поля.
  $values['email'] = empty($_COOKIE['email_value']) ? '' : strip_tags($_COOKIE['email_value']);
  $values['date'] = empty($_COOKIE['date_value']) ? '' : strip_tags($_COOKIE['date_value']);
  $values['sex'] = empty($_COOKIE['sex_value']) ? '' : strip_tags($_COOKIE['sex_value']);
  $values['limbs'] = empty($_COOKIE['limbs_value']) ? '' : strip_tags($_COOKIE['limbs_value']);
  $values['abilities'] = empty($_COOKIE['abilities_value']) ? '' : strip_tags($_COOKIE['abilities_value']);
  $values['biography'] = empty($_COOKIE['biography_value']) ? '' : strip_tags($_COOKIE['biography_value']);
  // Если нет предыдущих ошибок ввода, есть кука сессии, начали сессию и
  // ранее в сессию записан факт успешного логина.
  if (empty($errors) && !empty($_COOKIE[session_name()]) &&
      session_start() && !empty($_SESSION['login'])) {
    // TODO: загрузить данные пользователя из БД          !!!!!!!!!!
    // и заполнить переменную $values,
    // предварительно санитизовав.

    $user = 'u15824';
    $pass = '4568020';
    $db = new PDO('mysql:host=localhost;dbname=u15824', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
    
    // Подготовленный запрос. Не именованные метки.
    try {
        $stmt = $db->prepare("SELECT name, year, sex, limbs, abilities, biography FROM application1 WHERE email = ?");
        $stmt -> execute($_POST['email']);
        $values = $stmt->fetchAll();
    }
    catch(PDOException $e){
        print('Error : ' . $e->getMessage());
        exit();
    } 

  $values['fio'] = empty($_COOKIE['fio_value']) ? '' : strip_tags($_COOKIE['fio_value']);
  // TODO: аналогично все поля.
  $values['email'] = empty($_COOKIE['email_value']) ? '' : strip_tags($_COOKIE['email_value']);
  $values['date'] = empty($_COOKIE['date_value']) ? '' : strip_tags($_COOKIE['date_value']);
  $values['sex'] = empty($_COOKIE['sex_value']) ? '' : strip_tags($_COOKIE['sex_value']);
  $values['limbs'] = empty($_COOKIE['limbs_value']) ? '' : strip_tags($_COOKIE['limbs_value']);
  $values['abilities'] = empty($_COOKIE['abilities_value']) ? '' : strip_tags($_COOKIE['abilities_value']);
  $values['biography'] = empty($_COOKIE['biography_value']) ? '' : strip_tags($_COOKIE['biography_value']);
    printf('Вход с логином %s, uid %d', $_SESSION['login'], $_SESSION['uid']);
  }

  // Включаем содержимое файла form.php.
  // В нем будут доступны переменные $messages, $errors и $values для вывода 
  // сообщений, полей с ранее заполненными данными и признаками ошибок.
  include('form.php');
}
// Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.
else {
  // Проверяем ошибки.
  $errors = FALSE;
  if (!preg_match("#^[А-Яа-яЁё]+\s[А-Яа-яЁё]+\s[А-Яа-яЁё]+$#i", $_POST['fio'])) {
    // Выдаем куку на день с флажком об ошибке в поле fio.
    setcookie('fio_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    // Сохраняем ранее введенное в форму значение на месяц.
    setcookie('fio_value', $_POST['fio'], time() + 30 * 24 * 60 * 60);
  }

// *************
// TODO: тут необходимо проверить правильность заполнения всех остальных полей.
// Сохранить в Cookie признаки ошибок и значения полей.
// *************
if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
  // Выдаем куку на день с флажком об ошибке в поле fio.
  setcookie('email_error', '1', time() + 24 * 60 * 60);
  $errors = TRUE;
}
else {
  // Сохраняем ранее введенное в форму значение на месяц.
  setcookie('email_value', $_POST['email'], time() + 30 * 24 * 60 * 60);
}
if (empty($_POST['date'])) {
  // Выдаем куку на день с флажком об ошибке в поле fio.
  setcookie('date_error', '1', time() + 24 * 60 * 60);
  $errors = TRUE;
}
else {
  // Сохраняем ранее введенное в форму значение на месяц.
  setcookie('date_value', $_POST['date'], time() + 30 * 24 * 60 * 60);
}
if (empty($_POST['sex'])) {
  // Выдаем куку на день с флажком об ошибке в поле fio.
  setcookie('sex_error', '1', time() + 24 * 60 * 60);
  $errors = TRUE;
}
else {
  // Сохраняем ранее введенное в форму значение на месяц.
  setcookie('sex_value', $_POST['sex'], time() + 30 * 24 * 60 * 60);
}
if (empty($_POST['limbs'])) {
  // Выдаем куку на день с флажком об ошибке в поле fio.
  setcookie('limbs_error', '1', time() + 24 * 60 * 60);
  $errors = TRUE;
}
else {
  // Сохраняем ранее введенное в форму значение на месяц.
  setcookie('limbs_value', $_POST['limbs'], time() + 30 * 24 * 60 * 60);
}
if (empty($_POST['biography'])) {
  // Выдаем куку на день с флажком об ошибке в поле fio.
  setcookie('biography_error', '1', time() + 24 * 60 * 60);
  $errors = TRUE;
}
else {
  // Сохраняем ранее введенное в форму значение на месяц.
  setcookie('biography_value', $_POST['biography'], time() + 30 * 24 * 60 * 60);
}
setcookie('abilities_value', serialize($_POST['abilities']), time() + 30 * 24 * 60 * 60);
if (empty($_POST['check'])) {
  // Выдаем куку на день с флажком об ошибке в поле fio.
  setcookie('check_error', '1', time() + 24 * 60 * 60);
  $errors = TRUE;
}
else {
  // Сохраняем ранее введенное в форму значение на месяц.
  setcookie('check_value', $_POST['check'], time() + 30 * 24 * 60 * 60);
}




  if ($errors) {
    // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
    header('Location: index.php');
    exit();
  }
  else {
    // Удаляем Cookies с признаками ошибок.
    setcookie('fio_error', '', 100000);
    // TODO: тут необходимо удалить остальные Cookies.
    setcookie('fio_value', '', 100000);
    setcookie('email_error', '', 100000);
    setcookie('email_value', '', 100000);
    setcookie('date_error', '', 100000);
    setcookie('date_value', '', 100000);
    setcookie('sex_error', '', 100000);
    setcookie('sex_value', '', 100000);
    setcookie('limbs_error', '', 100000);
    setcookie('limbs_value', '', 100000);
    setcookie('biography_error', '', 100000);
    setcookie('biography_value', '', 100000);
    setcookie('check_error', '', 100000);
    setcookie('check_value', '', 100000);
    setcookie('abilities_value', '', 100000);
  }

  switch($_POST['sex']) {
    case 'm': {
        $sex='m';
        break;
    }
    case 'f':{
        $sex='f';
        break;
    }
};

switch($_POST['limbs']) {
    case '1': {
        $limbs='1';
        break;
    }
    case '2':{
        $limbs='2';
        break;
    }
    case '3':{
        $limbs='3';
        break;
    }
    case '4':{
        $limbs='4';
        break;
    }
};

$abilities = serialize($_POST['abilities']);

  // Проверяем меняются ли ранее сохраненные данные или отправляются новые.
  if (!empty($_COOKIE[session_name()]) &&
      session_start() && !empty($_SESSION['login'])) {
    // TODO: перезаписать данные в БД новыми данными,
    // кроме логина и пароля.
    $user = 'u15824';
    $pass = '4568020';
    $db = new PDO('mysql:host=localhost;dbname=u15824', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
    
    // Подготовленный запрос. Не именованные метки.
    try {
        $stmt = $db->prepare("UPDATE application1 SET name = ?, year = ?, sex = ?, limbs = ?, abilities = ?, biography = ? WHERE email = ?");
        $stmt -> execute(array($_POST['fio'],$_POST['date'],$sex,$limbs,$abilities,$_POST['biography'],$_POST['email']));
    }
    catch(PDOException $e){
        print('Error : ' . $e->getMessage());
        exit();
    } 
  }
  else {
    // Генерируем уникальный логин и пароль.
    // TODO: сделать механизм генерации, например функциями rand(), uniquid(), md5(), substr().
    $login = $_POST['email'];
    $password = '123';
    // Сохраняем в Cookies.
    setcookie('login', $login);
    setcookie('password', $password);

    // TODO: Сохранение данных формы, логина и хеш md5() пароля в базу данных.
    $user = 'u15824';
    $pass = '4568020';
    $db = new PDO('mysql:host=localhost;dbname=u15824', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
    
    // Подготовленный запрос. Не именованные метки.
    try {
        $stmt = $db->prepare("INSERT INTO application1 SET name = ?, email = ?, year = ?, sex = ?, limbs = ?, abilities = ?, biography = ?, pass = ?");
        $stmt -> execute(array($_POST['fio'],$_POST['email'],$_POST['date'],$sex,$limbs,$abilities,$_POST['biography'], $password));
    }
    catch(PDOException $e){
        print('Error : ' . $e->getMessage());
        exit();
    }
  }

  // Сохраняем куку с признаком успешного сохранения.
  setcookie('save', '1');

  // Делаем перенаправление.
  header('Location: ./');
}
